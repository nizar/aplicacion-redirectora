from flask import Flask, redirect
import random
import requests

app = Flask(__name__)

# Lista de URLs externas para redireccionar
external_urls = [
    "https://www.ejemplo1.com",
    "https://www.ejemplo2.com",
    "https://www.ejemplo3.com",
]

@app.route('/')
def index():
    # Simula un código de redirección (3xx)
    return redirect(get_random_url(), code=random.choice([301, 302, 303, 307, 308]))

def get_random_url():
    # Elige una URL aleatoria de la lista de URLs externas
    return random.choice(external_urls)

if __name__ == '__main__':
    app.run(debug=True)
